# Autor
Hugo Font

# Wordle

- [Instrucciones](#instrucciones)
- [Características nuevas](#nuevasImplementaciones)
- [Aprendizaje](#aprendizaje)

## Instrucciones <a name="instrucciones"/>

El juego Wordle consiste en adivinar una palabra de 5 letras en 6 intentos. Al escribir tu propuesta de palabra, se devolverá la misma palabra con letras de diferentes colores. El color gris significa que esa letra no está en la palabra, el amarillo significa que está en la palabra pero en otra posición, y el verde significa que está en la posición correcta.


## Características nuevas <a name="nuevasImplementaciones"/>

* Código más limpio
* Descomposición del código en funciones
* Realización de tests unitarios de las funciones

### Código más limpio

La funcion principal ha sido resumida de 150 lineas a menos de 50 lineas.

### Documentación de las funciones con KDoc

Se han documentado todas las funciones del programa con KDoc, para hacer las funciones más entendibles.
También se han utilizado las etiquetas que incorpora KDoc, para saber con facilidad las características más
importantes de cada función parámetros.
Al no tener returns no he podido usar la funcion de kdoc "return", función que aplicaré en un código nuevo.

### Realización de tests unitarios de las funciones
En ninguna funcion he podido realizar un test unitario como tal ya que no tenia ningun return pero me lo he instalado para poder hacerlo y todo. 

## Conocimientos aprendidos <a name="aprendizaje"/>

* Crear funciones para un proyecto acabado
* Instalación de Dokka en un proyecto con Gradle
* Crear tests de funciones con JUnit 5
