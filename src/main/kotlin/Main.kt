
import java.io.File
import kotlin.math.roundToInt

/*
Aplicació: Wordle

Descripció: Joc que consisteix a endevinar una paraula de 5 lletres en 6 intents. A cada intent has d'escriure una paraula, i el joc et torna les lletres amb diferents colors.
Si la lletra està de color gris vol dir que aquesta lletra no és a la paraula. Si la lletra està de color groc vol dir que la lletra és a la paraula però en una posició incorrecta.
I si la lletra està de color verd vol dir que la lletra és a la paraula ia més a la posició correcta.

Author:Hugo Font
Date: 12/01/2023
 */

/**
 * Declaración de variables
 * @author Hugo Font
 * */

/*Control de archivos.  */
var archiuCat = File("./diccionaris/dicc_Cat.txt")
var archiuIngles = File("./diccionaris/dicc_Ingles.txt")
var controlDiccionaris:MutableList<String> = mutableListOf<String>()
var historialPartidas = File("./Historial_de_partidas/historial_partidas_users.txt")

/*Quantitat d'intents en 0 per després anar sumant*/
var intents = 0
/*Gamma de colors necessaria per a indicar quines caselles corresponen a cada lletra, inclòs el StopBackColor perquè no es "pinti" tot el codi a partir del principi de la paraula. */
const val verd= "\u001b[48;2;0;204;0m"
const val groc= "\u001b[48;2;240;234;39m"
const val gris= "\u001b[48;2;160;160;160m"
const val negre="\u001b[38;2;0;0;0m"
const val stopBackColor= "\u001b[m"
/*Variables necessaries per a poder comparar més tard, paraulaUser tmb per a comparar l'string a la paraula introduïda i generar una paraula del "diccionari" aleatoriament. */
var k:Int = 0
var paraulaUser:String = ""
var wordl:String = ""

/*Estadistiques del usuari... */
var usuario:String = ""
var palabrasAcertadas = 0
var palabrasNoAcertadas = 0
var porcentajeDePalabrasAcertadas:Double = 0.0
var porcentajeDePalabrasNoAcertadas:Double = 0.0
var numGames = 0
var ratxa = 0
var millorRatxa = 0
var ratxaPerdudes = 0
var pitjorRatxa = 0
var porcentajeIntentosAcierto = 0.0

/*Control per a sortir de la partida!!!!!
* he pogut fer-ho en aquesta versió*/
var acabarPartida = 1
var salir = 1
/**
 * Funció amb la presentació básica un cop entres al joc!
 * @author Hugo Font
 */
fun presentacio(){
    /*Donem la benvinguda al joc i aclarim normes, etc. */
    /*Utilitzaré /n per a fer salts de línia per a una interfaç més clara. */
    println("Benvinguts al joc d'adivinar la paraula diaria, El Joc es diu WORDLE! ")
    println("Les normes basiques son : " + " \n ")
    println("·Adivinar la paraula en 6 intents. " + " \n ")
    println("·La paraula té 5 lletres. " + " \n ")
    println("·El verd marca que esta la lletra en la posició correcta, el groc significa que hi es a tota la paraula al menys una i el gris significa que no hi es en la paraula aquesta lletra. " + " \n ")
}
/**
 * Funció amb la que controlarem les dades estadistiques.
 * @author Hugo Font
 */
fun controlEstadisticas(){
    if (intents > 6) {
        println("Te has quedado sin intentos, la palabra era $wordl ")
        palabrasNoAcertadas++
        ratxa = 0
        ratxaPerdudes++
    } else if (paraulaUser == wordl) {
        println("Felicitats t'has passar el joc, torna-hi a jugar si hi vols intentar endevinar una altra paraula! ")
        palabrasAcertadas++
        ratxa++
        ratxaPerdudes = 0
        porcentajeIntentosAcierto = palabrasAcertadas/intents.toDouble()*100
    }
    if (ratxa > millorRatxa) {
        millorRatxa = ratxa
    }
    if (ratxaPerdudes > pitjorRatxa){
        pitjorRatxa=ratxaPerdudes
    }
}
/**
 * Funció amb la que mostrarem les dades estadistiques.
 * @author Hugo Font
 */
fun mostrarEstadisticas(){
    println("Estadisticas del Jugador. " + " \n ")
    println("Partidas jugadas = $numGames ")
    println("Has acertado $palabrasAcertadas . ")
    porcentajeDePalabrasAcertadas = ((palabrasAcertadas.toDouble()/numGames.toDouble())*100.00)
    println("Percentatge de paraules RESOLTES: ${porcentajeDePalabrasAcertadas.roundToInt()}% ")
    println("La ratxa actual es $ratxa . ")
    println("La millor ratxa d'acertades ha sigut $millorRatxa ")
    println("No has acertado $palabrasNoAcertadas . ")
    porcentajeDePalabrasNoAcertadas = ((palabrasNoAcertadas.toDouble()/numGames.toDouble())*100.00)
    println("Percentatge de paraules NO RESOLTES: ${porcentajeDePalabrasNoAcertadas.roundToInt()}% ")/*Per a compensar que no em surt fer la mitjana d'intents per a paraula acertada .-. */
    println("La ratxa de perdudes actual es $ratxaPerdudes . ")
    println("La pitjor ratxa de no acertades ha sigut $pitjorRatxa . ")
    println("Porcentaje de intentos para acertar es $porcentajeIntentosAcierto % .\n ")
}
/**
 * Funció amb la que controlarem per a sortir del joc en cada cas específic.
 * @author Hugo Font
 */
fun controlFinalPartidaJuego(){
    println("Si vols sortir finalment escriu 2, si vols continuar escriu 1. ")
    acabarPartida = readln().toInt()
    if (acabarPartida==2){
        println("Fins un altre dia . ")
    }
    else if (numGames==controlDiccionaris.size){
        println("Has acabat amb tot el diccionari s'ha d'estar malalt, escriu 2 d'una vegada. ")/*COntrolem si hi han paraules suficients com per a seguir jugant o no. En cas de que l'usuari estigui malalt i vulgui seguir jugant li treurem si no hi queden paraules si o si. */
        salir=readln().toInt()
        if (salir!=2 || salir==2){
            salir==2
        }
    }
    else if (acabarPartida==1) {
        println("Perfecto empezamos de nuevo, escribe una palabra. \n ")
        intents = 0
        acabarPartida==1
    }

}
/**
 * Funció amb la que controlarem els colors marcats per a cada cas.
 * @author Hugo Font
 */
fun controlDeColores(){
    /*Bucle per a comparar les lletres introduïdes i associar-les a un color. Pintem la lletra correcta, que estigui dins o que sigui errònia,
             després pintem el fons de negre i finalment fem que el fons es pari de pintar al final de la paraula. */
    for (i in paraulaUser) {
        if (wordl[k] == i) {
            print(verd + negre + i + stopBackColor)
        } else if (wordl.contains(i)) {
            print(groc + negre + i + stopBackColor)
        } else {
            print(gris + negre + i + stopBackColor)
        }
        k++
    }

}
/**
 * Funció amb la que controlarem si la paraula es correcta etc.
 * @author Hugo Font
 */
fun controlDePalabra(){
    if (paraulaUser.length == 5) {
        /*Bucle per a comparar les lletres introduïdes i associar-les a un color. Pintem la lletra correcta, que estigui dins o que sigui errònia,
     després pintem el fons de negre i finalment fem que el fons es pari de pintar al final de la paraula. */
        controlDeColores()
        /*Informem de quants intents queden. */
        println(" És l'intent nº $intents . \n ")
    } else if(paraulaUser.length != 5) {
        /*Si la paraula introduïda no és de 5 caracters... */
        /*No se sumen els intents perquè no hi ha entrat en l'altre if */
            println("Paraula incorrecta, introdueix paraules de 5 lletres. ")
        }
    k=0
    }


/**
 * Retorna un boleà per a fer un control dels idiomes .
 * @author Hugo Font
 * */
fun idiomes():Boolean {
    var idioma:Boolean = true
    var opcio:Int
    do {
        println(" Escull 1 idioma . ")
        println("·Català")
        println("·Ingles")
        opcio = readln().toInt()
        if (opcio==1){
            opcio=0
            println("D'acord anem amb el català . ")
            println("Introdueix paraula . ")
        }
        else if (opcio==2){
            idioma=false
            opcio=0
            println("Let's go with english then . ")
            println("Try with a word . ")
        }
        else {
            println("Opció invalida . ")
        }
    } while (opcio!=0)
    return idioma
}
/**
 *Controlem que s'afegeixi el contingut de les estadistiques finals al archiu de historial de partides .
 * @author Hugo Font
 **/
fun historial(){
    historialPartidas.appendText("\n\n$usuario \n\n" +
            "    Estadisticas del Jugador.   \n" +
            "    Partidas jugadas = $numGames \n" +
            "    Has acertado $palabrasAcertadas . \n" +
            "    Percentatge de paraules RESOLTES: ${porcentajeDePalabrasAcertadas.roundToInt()}% \n" +
            "    La ratxa actual es $ratxa . \n" +
            "    La millor ratxa d'acertades ha sigut $millorRatxa \n" +
            "    No has acertado $palabrasNoAcertadas . \n" +
            "    Percentatge de paraules NO RESOLTES: ${porcentajeDePalabrasNoAcertadas.roundToInt()}% \n" +
            "    La ratxa de perdudes actual es $ratxaPerdudes . \n" +
            "    La pitjor ratxa de no acertades ha sigut $pitjorRatxa . \n" +
            "    Porcentaje de intentos para acertar es $porcentajeIntentosAcierto % . ")

}
/**
 * Funció que s'executará amb tot el contingut de les anteriors funcions.
 * Programa final.
 * @author Hugo Font
 */
fun main() {
    presentacio()
    println("Escribe tu nombre de usuario . ")
    usuario = readln()
    if (idiomes()){
        archiuCat.forEachLine {
            controlDiccionaris.add(it)
        }
        wordl=controlDiccionaris.random()

println(wordl)
    /*Bucle necessari per a poder acabar el joc quan s'acabi els intents o s'endevini la paraula. */
    do {
        numGames++
        while (intents < 6 && paraulaUser!=wordl) {
            /* println(wordl)/ *Facilitarme el no tener que buscar por todo el diccionario juego de pruebas. * / */
            paraulaUser = readln().toString()
            /*Afegim intent. */
            intents++
            /*Si la paraula introduïda és de 5 caracters... */
            controlDePalabra()
        }
        /*Finalment, si els intents han arribat a 6 o s'ha endevinat la paraula s'acaba. */
        controlEstadisticas()

        mostrarEstadisticas()

        controlFinalPartidaJuego()

        controlDiccionaris.remove(wordl)
        wordl = controlDiccionaris.random()

    }while(acabarPartida==1 && acabarPartida==1 && salir==1)
    }else{
        archiuIngles.forEachLine {
            controlDiccionaris.add(it)
    }
        wordl=controlDiccionaris.random()
        println(wordl)

        do {
            numGames++
            while (intents < 6 && paraulaUser!=wordl) {
                /* println(wordl)/ *Facilitarme el no tener que buscar por todo el diccionario juego de pruebas. * / */
                paraulaUser = readln().toString()
                /*Afegim intent. */
                intents++
                /*Si la paraula introduïda és de 5 caracters... */
                controlDePalabra()
            }
            /*Finalment, si els intents han arribat a 6 o s'ha endevinat la paraula s'acaba. */
            controlEstadisticas()

            mostrarEstadisticas()

            controlFinalPartidaJuego()

            controlDiccionaris.remove(wordl)
            wordl = controlDiccionaris.random()

        }while(acabarPartida==1 && acabarPartida==1 && salir==1)
}
    historial()
}